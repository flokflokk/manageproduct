import React, { Component } from 'react'
import { connect } from 'react-redux'
import "../../../../css/ProductItem.css"
import { addToCart } from '../../../../actions/cart/cartActions'

class ProductItem extends Component {

  handleClick = (id) => {
    this.props.addToCart(id);
  }

  render() {
    const { product, to, } = this.props
      return (
        <div className="columns">

          <div className="card">
            <div className="columns ">
              <img className="imgP" src={product.coverImage} alt={product.name} />
              <div className="content column is-one-quarter">
                <strong>Product Feature</strong>
                <p>{product.feature}</p>
                <p>{product.id}</p>
              </div>
              <div className="content column is-one-quarter">
                <strong>Product Benefit</strong>
                <p>{product.benefit}</p>
                <button className="btn-floating halfway-fab waves-effect waves-light red"
                  onClick={() => { this.handleClick(product.id) }}>add</button>
              </div>
              <div className="content column quantity ">
                <p>{product.price} B </p>
                <strong>Quatinty</strong>
                <input type="number" name="quantity" min="1" max="99"></input>
                <p>{product.quantity} piece </p>

              </div>
            </div>
          </div>

        </div>

      )
  }
}

const mapDispatchToProps = (dispatch) => {

  return {
    addToCart: (id) => { dispatch(addToCart(id)) }
  }
}


export default connect(mapDispatchToProps)(ProductItem)