import React from "react";
import { Route, Switch } from "react-router-dom";

import cart from "./Cart";

export default () => (
    <Switch>
      <Route exact path="/cart" component={cart} />
    </Switch>
  );
  